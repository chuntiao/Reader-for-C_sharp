# Reader for C_sharp
从控制台读取数据,可用空格或 '\t' 隔开

## 例如

你在控制台输入一行 15 17.0 ppp
并在代码中写：

* int a=Reader.nextInt();          //a=15
* float b=Reader.nextFloat();    //b=17.0
* string s=Reader.nextStr();      //s="ppp"

即可轻松获取变量的值。